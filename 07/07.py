from utils.decorators import time_it
from collections import defaultdict
import re

with open('input') as f:
    puzzle_input = f.readlines()


@time_it
def part_one(n):
    deps = defaultdict(set)
    nodes = set()
    p = re.compile(r'^Step (.) mu.*ep (.) ca.*')
    for line in n:
        m = p.match(line)
        deps[m.group(2)].add(m.group(1))
        nodes.add(m.group(1))
        nodes.add(m.group(2))
    nodes_rem = sorted(nodes)
    done = set()
    order = ''
    while nodes_rem:
        for c in nodes_rem:
            if not deps[c]-done:
                order += c
                done.add(c)
                nodes_rem.remove(c)
                break
    return order


@time_it
def part_two(n, workers=5, step=60):
    deps = defaultdict(set)
    nodes = set()
    p = re.compile(r'^Step (.) mu.*ep (.) ca.*')
    for line in n:
        m = p.match(line)
        deps[m.group(2)].add(m.group(1))
        nodes.add(m.group(1))
        nodes.add(m.group(2))
    nodes_rem = sorted(nodes)
    done_time = {}
    busy_until = [0] * workers
    order = ''
    curr_time = 0
    while nodes_rem:
        if all(t > curr_time for t in busy_until):
            curr_time = min(busy_until)
        for c in nodes_rem:
            if all(d in done_time and done_time[d] <= curr_time for d in deps[c]):
                order += c
                for i, b in enumerate(busy_until):
                    if b <= curr_time:
                        busy_until[i] = curr_time + step + ord(c) - 64
                        done_time[c] = busy_until[i]
                        break
                nodes_rem.remove(c)
                del deps[c]
                break
        else:
            curr_time = min(t for t in busy_until if t > curr_time)
    return max(busy_until)


test_one = [
    'Step A must be finished before step B can begin.',
    'Step A must be finished before step D can begin.',
    'Step B must be finished before step E can begin.',
    'Step C must be finished before step F can begin.',
    'Step D must be finished before step E can begin.',
    'Step C must be finished before step A can begin.',
    'Step F must be finished before step E can begin.'
]

assert part_one(test_one) == 'CABDFE'

assert part_two(test_one, workers=2, step=0) == 15

print('Part one:', part_one(puzzle_input))

print('Part one:', part_two(puzzle_input))
