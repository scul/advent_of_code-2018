from utils.decorators import time_it

import re

with open('input') as f:
    puzzle_input = f.readline().strip()


def place_marble(n, marble, i):
    if i is None:
        max_i = n.index(max(n))
        n.insert((max_i + 1) % (len(n))+1, marble)
        return n
    n.insert(i + 2, marble)
    return n


@time_it
def solve(n, part_two=False):
    m = re.match(r'^(\d+) players; last marble is worth (\d+) points', n)
    num_players = int(m.group(1))
    last_marble = int(m.group(2))
    if part_two:
        last_marble *= 100

    player_scores = [0] * num_players

    marbles = [0, 1]
    curr_index = 1
    for i in range(2, last_marble+1):
        if i % 23 == 0:
            player_scores[i % num_players] += i
            rem = (curr_index - 7 + len(marbles)) % len(marbles)
            player_scores[i % num_players] += marbles.pop(rem)
            curr_index = rem

        else:
            place = (curr_index + 1) % len(marbles) + 1
            marbles.insert(place, i)
            curr_index = place

    return max(player_scores)


test_one = [
    ('9 players; last marble is worth 25 points', 32),
    ('10 players; last marble is worth 1618 points', 8317),
    ('17 players; last marble is worth 1104 points', 2764),
    ('21 players; last marble is worth 6111 points', 54718),
    ('30 players; last marble is worth 5807 points', 37305),
    ('13 players; last marble is worth 7999 points', 146373)
]


for test in test_one:
    assert solve(test[0]) == test[1]

print('Part one:', solve(puzzle_input))

print('Part two:', solve(puzzle_input, True))