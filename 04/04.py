from utils.decorators import time_it
from collections import defaultdict, Counter

with open('input') as f:
    puzzle_input = f.readlines()


@time_it
def part1(n):
    guards = defaultdict(list)
    times = defaultdict(int)
    start, end = 0, 0
    guard_id = None
    for line in sorted(n):
        time, event = line.split('] ')
        minute = int(time[-2:])

        if 'Guard' in event:
            guard_id = int(event.split('#')[1].split(' ')[0])
        elif 'falls' in event:
            start = minute
        elif 'wakes' in event:
            end = minute
            for x in range(start, end):
                guards[guard_id].append(x)
            times[guard_id] += end - start

    guard, time = max(times.items(), key=lambda i: i[1])
    minute, count = Counter(guards[guard]).most_common(1)[0]

    count_max = -1
    minute_max = -1
    guard2 = None
    for g in guards:
        minute2, count = Counter(guards[g]).most_common(1)[0]
        if count > count_max:
            count_max = count
            minute_max = minute2
            guard2 = g

    return guard * minute, guard2 * minute_max


'''
@time_it
def part2(guards, times):
    pass
'''


test_one = [
    '[1518-11-01 00:00] Guard #10 begins shift',
    '[1518-11-01 00:05] falls asleep',
    '[1518-11-01 00:25] wakes up',
    '[1518-11-01 00:30] falls asleep',
    '[1518-11-01 00:55] wakes up',
    '[1518-11-01 23:58] Guard #99 begins shift',
    '[1518-11-02 00:40] falls asleep',
    '[1518-11-02 00:50] wakes up',
    '[1518-11-03 00:05] Guard #10 begins shift',
    '[1518-11-03 00:24] falls asleep',
    '[1518-11-03 00:29] wakes up',
    '[1518-11-04 00:02] Guard #99 begins shift',
    '[1518-11-04 00:36] falls asleep',
    '[1518-11-04 00:46] wakes up',
    '[1518-11-05 00:03] Guard #99 begins shift',
    '[1518-11-05 00:45] falls asleep',
    '[1518-11-05 00:55] wakes up'
]
p1, p2 = part1(test_one)
assert p1 == 240
assert p2 == 4455

print(f'Part 1, 2: {part1(puzzle_input)}')
# print(f'Part 2: {part2(puzzle_input)}')
