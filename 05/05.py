from utils.decorators import time_it

with open('input') as f:
    puzzle_input = f.readline().strip()


# @time_it
def part1(n):
    for i in range(len(n) - 1, 0, -1):
        a, b = n[i], n[i-1]
        if a != b and a.lower() == b.lower():
            if i == len(n)-1:
                n = ' ' + n[:i-1]
            else:
                n = n[:i-1] + n[i+1:]
    return len(n.strip())


@time_it
def part2(n):
    return min(part1(n.replace(char, '').replace(char.upper(), '')) for char in set(n.lower()))


test_one = 'dabAcCaCBAcCcaDA'

assert part1('bAaB') == 0
assert part1(test_one) == 10
print(f'Part 1: {part1(puzzle_input)}')

assert part2(test_one) == 4
print(f'Part 2: {part2(puzzle_input)}')
