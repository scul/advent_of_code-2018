from utils.decorators import time_it

with open('input') as f:
    puzzle_input = [int(line) for line in f.readlines()]


def freq_delta(l):
    return sum(l)

@time_it
def find_first_dupe(l):
    seen = set()
    s = 0
    while True:
        for i in l:
            if s in seen:
                return s
            seen.add(s)
            s += i


assert freq_delta([1, 1, 1]) == 3
assert freq_delta([1, 1, -2]) == 0
assert freq_delta([-1, -2, -3]) == -6

print(freq_delta(puzzle_input))

assert find_first_dupe([1, -1]) == 0
assert find_first_dupe([+3, +3, +4, -2, -4]) == 10
assert find_first_dupe([-6, +3, +8, +5, -6]) == 5
assert find_first_dupe([+7, +7, -2, -7, -4]) == 14

print(find_first_dupe(puzzle_input))

