from utils.decorators import time_it
from collections import defaultdict
import re

with open('input') as f:
    puzzle_input = f.readlines()


id_re = re.compile(f'(#\d+) ')
area_re = re.compile(r'@ (\d+),(\d+):')
size_re = re.compile(r': (\d+)x(\d+)')
overlapping = defaultdict(int)


@time_it
def part1(n):
    count = 0
    for claim in n:
        x, y = map(int, area_re.search(claim).groups())
        w, h = map(int, size_re.search(claim).groups())
        for i in range(w):
            for j in range(h):
                overlapping[(x + i, y + j)] += 1
    for v in overlapping.values():
        if v > 1:
            count += 1
    return count


@time_it
def part2(n):
    for claim in n:
        id_ = id_re.search(claim).groups()[0]
        x, y = map(int, area_re.search(claim).groups())
        w, h = map(int, size_re.search(claim).groups())
        overlap = False
        for i in range(w):
            for j in range(h):
                if overlapping[(int(x) + i, int(y) + j)] > 1:
                    overlap = True
        if not overlap:
            return id_[1:]


test_one = [
    '#1 @ 1,3: 4x4',
    '#2 @ 3,1: 4x4',
    '#3 @ 5,5: 2x2'
]

assert part1(test_one) == 4
print(f'Part 1: {part1(puzzle_input)}')

assert part2(test_one) == '3'
print(f'Part 2: {part2(puzzle_input)}')
