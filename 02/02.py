with open('input') as f:
    puzzle_input = f.readlines()


def part1(n):
    two_count = 0
    three_count = 0
    for phrase in n:
        two_done = False
        three_done = False
        for char in set(phrase):
            if phrase.count(char) == 2 and not two_done:
                two_done = True
                two_count += 1
            if phrase.count(char) == 3 and not three_done:
                three_done = True
                three_count += 1
    return two_count * three_count


def part2(n):
    for word1 in n:
        for word2 in n:
            diff_count = sum(1 for i, j in zip(word1, word2) if i != j)
            if diff_count == 1:
                return ''.join(c for c in word1 if c in word2)


test_one = [
    'abcdef',
    'bababc',
    'abbcde',
    'abcccd',
    'aabcdd',
    'abcdee',
    'ababab'
]

assert part1(test_one) == 12
print(f'Part 1: {part1(puzzle_input)}')

test_two = [
    'abcde',
    'fghij',
    'klmno',
    'pqrst',
    'fguij',
    'axcye',
    'wvxyz'
]

assert part2(test_two) == 'fgij'
print(f'Part 2: {part2(puzzle_input)}')

