import os

for day in range(1, 26):
    day = f'{day:0>2.0f}'
    os.mkdir(day)
    with open(f'{day}/{day}.py', 'w') as f:
        f.write("with open('input') as f:\n    puzzle_input = f.readline()")
    with open(f'{day}/input', 'w') as f:
        f.write('')
    print(day)
