from utils.decorators import time_it

with open('input') as f:
    puzzle_input = f.read().split('\n')


def check_neighbors(pos, width, layout):
    """
    Analyzes cell's neighbors to determine if they are open, wooded, or lumber
    Returns a tuple with number of (open, trees, lumber) in order.

    :return: tuple
    """
    x, y = pos
    o = 0
    t = 0
    l = 0
    for i in (-1, 0, 1):
        for j in (-1, 0, 1):
            if i == 0 and j == 0:  # Don't count own cell
                continue
            if x + i < 0 or y + j < 0 or x + i >= width or y + j >= width:
                continue

            cell = layout[x+i + ((y + j) * width)]

            if cell == '.':
                o += 1
            elif cell == '|':
                t += 1
            elif cell == '#':
                l += 1

    return o, t, l


def print_grid(grid, w):
    for i, c in enumerate(grid):
        print(c, end='')
        if (i + 1) % w == 0 and i > 1:
            print()
    print()

@time_it
def part_one(n):
    layout = []
    w = len(n[0])
    h = len(n)
    for line in n:
        for cell in line:
            layout.append(cell)
    c = {}
    i = 0
    jump = False
    while i < 1000000000 - 1:
        # print_grid(layout, w)
        nxt = ['.'] * len(layout)
        for y in range(h):
            for x in range(w):
                neighbors = check_neighbors((x, y), w, layout)
                if layout[x + y * w] == '.':
                    if neighbors[1] >= 3:
                        nxt[x + y * w] = '|'
                    else:
                        nxt[x + y * w] = '.'
                elif layout[x + y * w] == '|':
                    if neighbors[2] >= 3:
                        nxt[x + y * w] = '#'
                    else:
                        nxt[x + y * w] = '|'
                elif layout[x + y * w] == '#':
                    if neighbors[1] >= 1 and neighbors[2] >= 1:
                        nxt[x + y * w] = '#'
                    else:
                        nxt[x + y * w] = '.'
        layout = nxt.copy()
        """
        Find where the repetition starts, then how often it repeats.
        Use those two data points to calculate what low numbered 
        generation will be the same pattern as generation 1000000000,
        then jump ahead based on that derived pattern. 
        """
        layout_hash = hash(''.join(layout))
        k = 0
        if layout_hash in c.values():
            for k, v in c.items():
                if layout_hash == v:
                    break

        c[i] = layout_hash
        
        if k > 0 and not jump:
            jump = True
            print(k, i)
            i = (1000000000 - k) // (i - k) * (i - k) + k
            print(i)
        else:
            i += 1

    # print_grid(layout, w)

    return layout.count('#') * layout.count('|')


test_one = [
    '.#.#...|#.',
    '.....#|##|',
    '.|..|...#.',
    '..|#.....#',
    '#.#|||#|#|',
    '...#.||...',
    '.|....|...',
    '||...#|.#|',
    '|.||||..|.',
    '...#.|..|.'
]

# p1 = part_one(test_one)
# print(p1)
# assert p1 == 1147

print(part_one(puzzle_input))
