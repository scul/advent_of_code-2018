from utils.decorators import time_it

with open('input') as f:
    puzzle_input = f.readlines()


@time_it
def part_one(n):
    offset = 5
    x = n[0].strip().split(': ')[1]
    state = ['.']*offset + [c for c in x] + ['.']*offset
    rules = {}
    for x in n[2:]:
        k, v = x.strip().split(' => ')
        rules[k] = v

    tot = 0
    gens = 0
    for i in range(20):
        next_state = ['.'] * len(state)
        x = state.count('#')
        if x != tot:
            gens = 0
            tot = x
        elif gens > 5:
            break
        elif x == tot:
            gens += 1
        else:
            gens = 0

        for x in range(len(state) - len(k)):
            for k, v in rules.items():
                if ''.join(state[x:x+5]) == k:
                    next_state[x + 2] = v
        if '#' in next_state[-5:]:
            next_state = next_state + ['.']*3
        state = next_state.copy()
    sum_ = 0
    for j, x in enumerate(state):
        if x == '#':
            sum_ += j - offset
    return sum_


@time_it
def part_two(n):
    offset = 5
    x = n[0].strip().split(': ')[1]
    state = ['.'] * offset + [c for c in x] + ['.'] * offset
    rules = {}
    for x in n[2:]:
        k, v = x.strip().split(' => ')
        rules[k] = v

    tot = 0
    gens = 0
    for i in range(1000):
        next_state = ['.'] * len(state)
        x = state.count('#')
        # print(x, tot, gens)

        if x != tot:
            gens = 0
            tot = x
        elif gens > 5:
            break
        elif x == tot:
            gens += 1
        else:
            gens = 0

        for x in range(len(state) - len(k)):
            for k, v in rules.items():
                if ''.join(state[x:x + 5]) == k:
                    next_state[x + 2] = v
        if '#' in next_state[-5:]:
            next_state = next_state + ['.'] * 3
        state = next_state.copy()
    sum_ = 0
    for j, x in enumerate(state):
        if x == '#':
            sum_ += j - offset + (50000000000 - i)
    return sum_


test_input = [
    'initial state: #..#.#..##......###...###',
    '',
    '...## => #',
    '..#.. => #',
    '.#... => #',
    '.#.#. => #',
    '.#.## => #',
    '.##.. => #',
    '.#### => #',
    '#.#.# => #',
    '#.### => #',
    '##.#. => #',
    '##.## => #',
    '###.. => #',
    '###.# => #',
    '####. => #'
]


p1 = part_one(test_input)
print(p1)
assert p1 == 325

print(f'Part 1: {part_one(puzzle_input)}')

print(f'Part 2: {part_two(puzzle_input)}')

