from utils.decorators import time_it

with open('input') as f:
    puzzle_input = f.readline().strip()

meta = []


def check_child(data, i):
    num_children = data[i]
    num_meta = data[i + 1]
    i += 2

    for x in range(num_children):
        i = check_child(data, i)

    for j in range(num_meta):
        meta.append(data[i+j])

    return i + num_meta


@time_it
def part_one(n):
    global meta
    meta = []
    n = [int(x) for x in n.split(' ')]
    check_child(n, 0)
    return sum(meta)


def parse(n, i):
    children = n[i]
    num_meta = n[i + 1]
    i += 2
    vals = {}
    for j in range(children):
        (i, val) = parse(n, i)
        vals[j + 1] = val
    local_meta = []
    for j in range(num_meta):
        local_meta.append(n[i + j])
        meta.append(n[i + j])
    i += num_meta
    if children:
        return i, sum(vals.get(m, 0) for m in local_meta)
    else:
        return i, sum(local_meta)


@time_it
def part_two(n):
    global meta
    meta = []
    n = [int(x) for x in n.split(' ')]
    ret = parse(n, 0)
    return ret[1]


test_one = '2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2'

assert part_one(test_one) == 138

assert part_two(test_one) == 66

print('Part one:', part_one(puzzle_input))

print('Part two:', part_two(puzzle_input))
