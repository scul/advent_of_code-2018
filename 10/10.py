from utils.decorators import time_it
import re

with open('input') as f:
    puzzle_input = f.readlines()


class Point:
    def __init__(self, pos, vel):
        self.pos = pos
        self.vel = vel

    def copy(self):
        return Point(self.pos.copy(), self.vel.copy())

    def move(self):
        self.pos[0] += self.vel[0]
        self.pos[1] += self.vel[1]


def calc_bounding_area(p):
    min_x = min([point for point in p], key=lambda pt: pt.pos[0]).pos[0]
    min_y = min([point for point in p], key=lambda pt: pt.pos[1]).pos[1]
    max_x = max([point for point in p], key=lambda pt: pt.pos[0]).pos[0]
    max_y = max([point for point in p], key=lambda pt: pt.pos[0]).pos[0]

    return (max_x - min_x) * (max_y - min_y)


def print_grid(p):
    min_x = min([point for point in p], key=lambda pt: pt.pos[0]).pos[0]
    min_y = min([point for point in p], key=lambda pt: pt.pos[1]).pos[1]
    max_x = max([point for point in p], key=lambda pt: pt.pos[0]).pos[0]
    max_y = max([point for point in p], key=lambda pt: pt.pos[1]).pos[1] + 1

    grid = []
    for x in range(max_y):
        grid.append([0] * 250)
        for y in range(250):
            for point in p:
                if point.pos[0] == y and point.pos[1] == x:
                    grid[x][y] = '#'
                else:
                    if grid[x][y] == 0:
                        grid[x][y] = ' '

    for line in grid[min_y:]:
        print(''.join(line[min_x:max_x+1]))


@time_it
def part_one(n):
    points = []
    min_bounding = 99999999
    min_index = 0
    min_grid = []
    pattern = re.compile(r'^position=<\s*(-?\d+),\s*(-?\d+)> velocity=<\s*(-?\d+),\s*(-?\d+)>$')
    for point in n:
        m = pattern.match(point)
        pos = list(map(int, (m.group(1), m.group(2))))
        vel = list(map(int, (m.group(3), m.group(4))))
        points.append(Point(pos, vel))

    for i in range(15000):
        b = calc_bounding_area(points)
        if b < min_bounding:
            min_bounding = b
            min_index = i
            min_grid = [point.copy() for point in points]
        for point in points:
            point.move()
    print(min_index)
    print_grid(min_grid)


test_one = [
    'position=< 9,  1> velocity=< 0,  2>',
    'position=< 7,  0> velocity=<-1,  0>',
    'position=< 3, -2> velocity=<-1,  1>',
    'position=< 6, 10> velocity=<-2, -1>',
    'position=< 2, -4> velocity=< 2,  2>',
    'position=<-6, 10> velocity=< 2, -2>',
    'position=< 1,  8> velocity=< 1, -1>',
    'position=< 1,  7> velocity=< 1,  0>',
    'position=<-3, 11> velocity=< 1, -2>',
    'position=< 7,  6> velocity=<-1, -1>',
    'position=<-2,  3> velocity=< 1,  0>',
    'position=<-4,  3> velocity=< 2,  0>',
    'position=<10, -3> velocity=<-1,  1>',
    'position=< 5, 11> velocity=< 1, -2>',
    'position=< 4,  7> velocity=< 0, -1>',
    'position=< 8, -2> velocity=< 0,  1>',
    'position=<15,  0> velocity=<-2,  0>',
    'position=< 1,  6> velocity=< 1,  0>',
    'position=< 8,  9> velocity=< 0, -1>',
    'position=< 3,  3> velocity=<-1,  1>',
    'position=< 0,  5> velocity=< 0, -1>',
    'position=<-2,  2> velocity=< 2,  0>',
    'position=< 5, -2> velocity=< 1,  2>',
    'position=< 1,  4> velocity=< 2,  1>',
    'position=<-2,  7> velocity=< 2, -2>',
    'position=< 3,  6> velocity=<-1, -1>',
    'position=< 5,  0> velocity=< 1,  0>',
    'position=<-6,  0> velocity=< 2,  0>',
    'position=< 5,  9> velocity=< 1, -2>',
    'position=<14,  7> velocity=<-2,  0>',
    'position=<-3,  6> velocity=< 2, -1>'
]

part_one(test_one)
print()
part_one(puzzle_input)
