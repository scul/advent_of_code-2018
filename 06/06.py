from utils.decorators import time_it
from collections import defaultdict

with open('input') as f:
    puzzle_input = f.readlines()


def dist(x1, y1, x2, y2):
    return abs(x1 - x2) + abs(y1 - y2)


@time_it
def part_one(n):
    """
    holy shit, this abomination works!  Slow, but works.
    """
    max_x = max(int(x.split(', ')[0]) for x in n)
    max_y = max(int(y.split(', ')[1]) for y in n)
    grid = []
    for x in range(max_x+1):
        grid.append([])
        for y in range(max_y+1):
            grid[x].append([])
            min_dist = 9999
            min_point = None
            for i, point in enumerate(n):
                p_x, p_y = [int(x) for x in point.split(', ')]
                d = dist(p_x, p_y, x, y)
                if d < min_dist:
                    min_dist = d
                    min_point = i
                elif d == min_dist:
                    min_point = '.'
                if d == 0:
                    min_point = '*'
            grid[x][y] = min_point

    exclude = set()
    for c in grid[0]:
        exclude.add(c)

    for c in grid[-1]:
        exclude.add(c)
    for line in grid:
        exclude.add(line[0])
        exclude.add(line[-1])

    sym = defaultdict(int)
    for line in grid:
        for c in line:
            if c in exclude:
                continue
            sym[c] += 1
    return sym[max(sym, key=lambda v: sym[v])]+1


@time_it
def part_two(n, max_d):
    max_x = max(int(x.split(', ')[0]) for x in n)
    max_y = max(int(y.split(', ')[1]) for y in n)
    region = 0
    for x in range(max_x + 1):
        for y in range(max_y + 1):
            tot_dist = 0
            for point in n:
                p_x, p_y = [int(x) for x in point.split(', ')]
                d = dist(p_x, p_y, x, y)
                tot_dist += d
            if tot_dist < max_d:
                region += 1

    return region


test_one = [
    '1, 1',
    '1, 6',
    '8, 3',
    '3, 4',
    '5, 5',
    '8, 9'
]

assert part_one(test_one) == 17

assert part_two(test_one, 32) == 16

print(part_one(puzzle_input))

print(part_two(puzzle_input, 10000))
