from PIL import Image, ImageDraw

mult = 8

size = 300 * mult


def fill_grid(n):
    grid = []
    for x in range(300):
        grid.append([])
        for y in range(300):
            grid[x].append(set_power(n, x + 1, y + 1))
    return grid.copy()

def set_power(n, x, y):
    rack_id = x + 10
    power = rack_id * y + n
    power *= rack_id
    power = (power // 100) % 10
    power -= 5
    return power


for a in range(1000):
    grid = fill_grid(a)
    im = Image.new(mode='RGB', size=(size, size))
    draw = ImageDraw.Draw(im, mode='RGB')
    for x in range(300):
        for y in range(300):
            c = grid[x][y]
            if c < 0:
                color = (255 - int((-c/5) * 255), 255, 255 - int((-c/5) * 255))
            elif c > 0:
                # print(c)
                color = (255, 255 - int((c/4) * 255), 255 - int((c/4) * 255))
            else:
                color = (0, 0, 255)
            draw.point([((x * mult) + i, (y * mult) + j)
                        for i in range(mult)
                        for j in range(mult)],
                       color)
    im.save(f'images/{a:0>4}.png', 'png')
    print(a)
